# sql-test-js

A Simple Javascript Connection Test to ensure connectivity to a Database from a container.

## Build the Code

```powershell
cd src
npm install
```

## Windows Containers Usage

Run the process directly

```powershell
docker-compose -f windows-compose.yml up -d db

$env:DB_HOST=$env:computername
$env:DB_PORT="9433"
$env:DB_NAME="master"
$env:DB_USERNAME="sa"
$env:DB_PASSWORD="PasswordAzure@123!"
node src/index.js
```

Build the Image

```
docker-compose -f windows-compose.yml build
```

Execute a Test

```powershell
docker-compose -f windows-compose.yml up
```

Execute a Test against an external Database
```powershell
docker run `
  -e DB_HOST="dbserver-XXXXX.database.windows.net" `
  -e DB_PORT="1433" `
  -e DB_NAME="master" `
  -e DB_USERNAME="ServerAdmin" `
  -e DB_PASSWORD="PasswordAzure@123!" `
  sql-test-js:windows
```

## Linux Containers Usage

Run the process directly

```powershell
docker-compose -f linux-compose.yml up -d db

$env:DB_HOST=$env:computername
$env:DB_PORT="9433"
$env:DB_NAME="master"
$env:DB_USERNAME="sa"
$env:DB_PASSWORD="PasswordAzure@123!"
node src/index.js
```

Build the Image

```
docker-compose -f linux-compose.yml build
```

Execute a Test

```powershell
docker-compose -f linux-compose.yml up
```

Execute a Test against an external Database
```powershell
docker run `
  -e DB_HOST="dbserver-XXXXX.database.windows.net" `
  -e DB_PORT="1433" `
  -e DB_NAME="master" `
  -e DB_USERNAME="ServerAdmin" `
  -e DB_PASSWORD="PasswordAzure@123!" `
  sql-test-js:linux
```