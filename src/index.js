var Connection = require('tedious').Connection;

// Create connection to database
var config = {
  userName: process.env.DB_USERNAME || 'sa',
  password: process.env.DB_PASSWORD || 'Password',
  server: process.env.DB_HOST || 'localhost',
  options: {
    database: process.env.DB_NAME || 'test',
    port: process.env.DB_PORT || '1433',
    encrypt: true
  }
}
var connection = new Connection(config);

// Attempt to connect and execute queries if connection goes through
connection.on('connect', function (err) {
  console.log(config);

  if (err) {
    console.log(`Connected to Server: ERROR.`)
    console.log(err)
    process.exit(1);
  } else {
    console.log(`Connected to Server: SUCCESS.`);
    process.exit(0);
  }
});