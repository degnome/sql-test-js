FROM danielscholl/alpine-node:latest

WORKDIR /app

COPY ./src/package.json /app/package.json

ENV NODE_ENV=production PORT=3000 DEL_PKGS="libgcc libstdc++" RM_DIRS=/usr/include

RUN apk add --no-cache curl make gcc g++ binutils-gold python linux-headers paxctl libgcc libstdc++ gnupg && \
  adduser -S appuser && \
  addgroup -S appuser && \
  npm install --unsafe-perm=true --production && \
  apk del curl make gcc g++ binutils-gold python linux-headers paxctl gnupg ${DEL_PKGS} && \
  rm -rf rm -rf ${RM_DIRS} && \
  chown -R appuser:appuser /app

COPY ./src/index.js /app/index.js

CMD ["node", "index.js"]