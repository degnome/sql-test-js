FROM stefanscherer/node-windows:8.9.2-nanoserver-1709

WORKDIR C:/app

COPY ./src/package.json C:/app/package.json
RUN npm install --unsafe-perm=true --production
COPY ./src/index.js C:/app/index.js

CMD ["node", "index.js"]